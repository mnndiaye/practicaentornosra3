		/*
		 * Clase para refactorizar
		 * Codificacion UTF-8
		 * Paquete: ra3.refactorizacion
		 * Se deben comentar todas las refactorizaciones realizadas,
		 * mediante comentarios de una linea o de bloque.
		 */
package ra3.Refactorizacion; 
//meter en paquete
		import java.*;
		import java.util.*;
		/**
		 * 
		 * @author fvaldeon
		 * @since 31-01-2018
		 */
		public class Refactorizar1 {
			//la clase siempre tiene que empezar  por mayuscula,as� que la llamar� Refactorizar1
			final static String SALUDO = ("Bienvenido al programa");
			public static void main(String[] args) {
				//Una sola declaracion por linea
				
				Scanner c = new Scanner(System.in);
				
				//en vez de cad pongo el valor Saludo que es "Bienvenidos al programa"
				System.out.println(SALUDO);
				System.out.println("Introduce tu dni");
				String dni = c.nextLine();
				System.out.println("Introduce tu nombre");
				String nombre = c.nextLine();
				
				int numeroA=7; int numeroB=16;
				
				int numeroc = 25;
				//uso parentesis 
				if(numeroA>numeroB||(numeroc%5)!=0 &&((numeroc*3)-1)>(numeroB/numeroc))
					//despues de if siempre se pone parentesis para mostrar que no es un error
				{
					System.out.println("Se cumple la condici�n");
				}
				
				//separar las operaciones
				numeroc = numeroA + (numeroB*numeroc) + (numeroB/numeroA);
				
				//meter todo en dos lineas
				String []diaSemana = {"Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo"};
	
				
				//cambiar el metodo a recorrer_diaSemana(String[]diasSemana)
				
				recorrer_diaSemana(diaSemana);
			}
			private static void recorrer_diaSemana(String vectordestrings[]) {
				String diasSemana = "";
				
				for(int dia=0; dia<7; dia++) 
				{
					System.out.println("El dia de la semana en el que te encuentras ["+(dia+1)+"-7] es el dia: " + vectordestrings[dia]);
						
				}
				
			}
		}
		
			

