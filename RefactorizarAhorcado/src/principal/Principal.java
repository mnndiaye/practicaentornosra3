package principal;

import java.io.File;
import java.util.Scanner;

public class Principal {
	private final static byte NUM_PALABRAS = 20;
	private final static byte FALLOS = 7;
	private static String[] palabras = new String[NUM_PALABRAS];

	public static void main(String[] args) {

		String palabraSecreta, ruta = "src\\palabras.txt";

		File fich = new File(ruta);
		Scanner inputFichero = null;

		try {
			inputFichero = new Scanner(fich);
			for (int i = 0; i < NUM_PALABRAS; i++) {
				palabras[i] = inputFichero.nextLine();
			}
		} catch (Exception e) {
			System.out.println("Error al abrir fichero: " + e.getMessage());
		} finally {
			if (fich != null && inputFichero != null)
				inputFichero.close();
		}

		Scanner input = new Scanner(System.in);

		palabraSecreta = palabras[(int) (Math.random() * NUM_PALABRAS)];

		char[][] caracteresPalabra = new char[2][];
		caracteresPalabra[0] = palabraSecreta.toCharArray();
		caracteresPalabra[1] = new char[caracteresPalabra[0].length];
		int fallos;
		
		boolean acertado;
		System.out.println("Acierta la palabra");
		do {
			//he metido todo en un metodo que sale fuera del main para los caracteres ocultos
			mostrarOculto(caracteresPalabra);
			
			System.out.println();
			
			fallos = caracteresElegidos(input, caracteresPalabra);
			
			//metodo que creo para mostrar el ahorcado
			mostrarAhorcado(fallos);
			
			//este metodo controlar� lo fallos realizados y si llegamos al l�mite nos mostrar� la palabra secreta
			acertado = controlFallos(palabraSecreta, caracteresPalabra, fallos);

		} while (!acertado && fallos < FALLOS);

		input.close();
	}

	public static boolean controlFallos(String palabraSecreta, char[][] caracteresPalabra, int fallos) {
		boolean acertado;
		if (fallos >= FALLOS) {
			System.out.println("Has perdido: " + palabraSecreta);
		}
		acertado = true;
		for (int i = 0; i < caracteresPalabra[1].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				acertado = false;
				break;
			}
		}
		if (acertado)
			System.out.println("Has Acertado ");
		return acertado;
	}

	private static int caracteresElegidos(Scanner input, char[][] caracteresPalabra) {
		int fallos;
		//meto los caracteres elegido en el m�todo que creo  para poderlos usar
		String caracteresElegidos = "";
		System.out.println("Introduce una letra o acierta la palabra");
		System.out.println("Caracteres Elegidos: " + caracteresElegidos);
		caracteresElegidos += input.nextLine().toUpperCase();
		
		 fallos = 0;

		boolean encontrado;
		for (int j = 0; j < caracteresElegidos.length(); j++) {
			encontrado = false;
			for (int i = 0; i < caracteresPalabra[0].length; i++) {
				if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
					caracteresPalabra[1][i] = '1';
					encontrado = true;
				}
			}
			if (!encontrado)
				fallos++;
		}
		return fallos;
	}

	private static void mostrarOculto(char[][] caracteresPalabra) {
		System.out.println("####################################");

		for (int i = 0; i < caracteresPalabra[0].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				System.out.print(" -");
			} else {
				System.out.print(" " + caracteresPalabra[0][i]);
			}
		}
	}

	private static void mostrarAhorcado(int fallos) {
		switch (fallos) {
		case 1:

			System.out.println("     ___");
			break;
		case 2:

			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 3:
			System.out.println("  ____ ");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 4:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 5:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 6:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println("     ___");
			break;
		case 7:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println(" A   ___");
			break;
		}
	}

}